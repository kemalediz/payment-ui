import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ActionSheetController} from "@ionic/angular";

@Component({
  selector: 'app-screen3',
  templateUrl: './screen3.page.html',
  styleUrls: ['./screen3.page.scss'],
})
export class Screen3Page implements OnInit {

  paymentMethod: string;
  isVisible:boolean;

  constructor(private activatedRoute: ActivatedRoute, public actionSheetController: ActionSheetController) { this.paymentMethod = 'Contactless'; }

  ngOnInit() {
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Select Payment Method',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Contactless',
        role: 'destructive',
        icon: 'logo-rss',
        handler: () => {
          this.paymentMethod = 'Contactless';
        }
      }, {
        text: 'QR Code',
        icon: 'qr-code-outline',
        handler: () => {
          this.paymentMethod = 'QR Code';
        }
      }, {
        text: 'Apple Pay',
        icon: 'logo-apple',
        handler: () => {
          this.paymentMethod = 'Apple Pay';
        }
      }]
    });
    await actionSheet.present();
  }
}

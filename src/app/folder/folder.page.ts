import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {ActionSheetController} from '@ionic/angular';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
  public folder: string;
  paymentMethod: string;

  constructor(private activatedRoute: ActivatedRoute, public actionSheetController: ActionSheetController) { this.paymentMethod = 'Contactless'; }

  ngOnInit() {
    this.folder = this.activatedRoute.snapshot.paramMap.get('id');
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Select Payment Method',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Contactless',
        role: 'destructive',
        icon: 'logo-rss',
        handler: () => {
          this.paymentMethod = 'Contactless';
        }
      }, {
        text: 'QR Code',
        icon: 'qr-code-outline',
        handler: () => {
          this.paymentMethod = 'QR Code';
        }
      }, {
        text: 'Apple Pay',
        icon: 'logo-apple',
        handler: () => {
          this.paymentMethod = 'Apple Pay';
        }
      }]
    });
    await actionSheet.present();
  }

}
